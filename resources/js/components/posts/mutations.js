import gpl from "graphql-tag";


export const CREATE_POST = gpl`
mutation($post: PostInput!) {
    createPosts(input: $post){
     id  
     title 
     body
     author_id
     date 
        
    }
}
`;

export const DELETE_POST = gpl`
mutation($id: ID!) {
    deletePost(id: $id){
        id
    }
}
`;