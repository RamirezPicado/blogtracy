import gpl from "graphql-tag";

export const POSTS_QUERY = gpl`
query {
    posts {
      paginatorInfo {
        hasMorePages
      }
      data {
        id
        title
        body
        author_id
        date
       
      }
    }
  }
`;
